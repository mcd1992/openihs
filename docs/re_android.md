# Reversing the Android Steam-Link app

## Logic Flow (Steam Link / Android [UDP 27036])
- Clients brodcast KERemoteClientBroadcastMsgDiscovery on broadcast/multicast addresses
- Servers respond with KERemoteClientBroadcastMsgStatus to broadcast/multicast and the client directly
- Client sends a KERemoteDeviceStreamingRequest to server with a unique request_id and the client_id (and optional client information)
  - If the client_id exists on the server already; the server will send a KERemoteDeviceProofRequest
- Server responds with KERemoteDeviceStreamingResponse to the same request_id and a ERemoteDeviceStreamingResult
  - KERemoteDeviceStreamingUnauthorized result causes the client to send a KERemoteDeviceAuthorizationRequest to the server with a token and encrypted request
    - Server will respond to auth. req. with KERemoteDeviceAuthorizationResponse [Result ERemoteDeviceAuthorizationResult]
    - Client will continue to pester the server with KERemoteDeviceStreamingRequest and KERemoteDeviceAuthorizationRequest until timeout or we get success
- Server will send a KERemoteDeviceProofRequest to client with challenge (Related to "SharedAuth" in ~/.local/share/Steam/userdata/sid/config/localconfig.vdf ?)
- Client will respond with KERemoteDeviceProofResponse and challenge response
- KERemoteDeviceStreamingSuccess result has the encrypted session key and new UDP port to send streaming data to
- Streaming data will be established on a different UDP port

## Android x86_64 Notes
- The java app 'entrypoint' is into libSDL2.so Java_org_libsdl_app_SDLActivity_nativeRunMain
  - Then it calls SDL_main in libmain.so
- The first argument passed to native functions is a `typedef const struct JNINativeInterface *JNIEnv;`
  - https://www.ibm.com/support/knowledgecenter/en/SSYKE2_8.0.0/com.ibm.java.vm.80.doc/docs/porting_jni.html
  - https://developer.android.com/training/articles/perf-jni#javavm-and-jnienv
  - https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/functions.html
  - https://github.com/jnr/jnr-ffi/blob/master/src/main/java/jnr/ffi/provider/jffi/JNINativeInterface.java#L37
- The second argument passed to native functions is `jobject` (a pointer to the implicit this object passed from the Java side)
- The symbol `JNI_OnLoad` will be called with `JNI_OnLoad(JavaVM *vm, void *reserved)`
- A lot of the source code is available in this repo! (CCrypto) https://github.com/ValveSoftware/GameNetworkingSockets/

## CServerManager::GetSecretKey(unsignedlonglong,CUtlBuffer*)
-

## Device Tokens
- CServerManager::GenerateDeviceToken()
- CServerManager::GetSecretKey(unsignedlonglong,CUtlBuffer*)
  - The private key is a 64byte hex string
  - Secret key is a 32byte binary representation of that?
  - CCrypto::GenerateRandomBlock(void*,int)
- CCrypto::GetSymmetricEncryptedSize(unsignedint)
- CCrypto::SymmetricEncrypt(unsignedcharconst*,unsignedint,unsignedchar*,unsignedint*,unsignedcharconst*,unsignedint)

## Streaming Request / Response
- Client->Server: Sends a ClientID and RequestID (And optional client info / device token)
- Server->Client: If the ClientID already exists on the server, server sends a Proof Request
- Server->Client: Respond with ClientID, InstanceID, RequestID, Result [Success: Port & Encrypted Session Key(48B)]

## Device Proof Request / Response
- Server->Client: Sends ClientID, InstanceID, and Challenge(16B)
- Client->Server: Responds with ClientID and Response(48B)

## Device Authorization Request / Response
Client->Server: Sends ClientID, DeviceToken(32B), DeviceName, EncryptedReq(256B)

## Android Trace / Flow
- sym.RunShell_int_char
- sym.CShellApplication::BInit_int__char
- CServerManagerNetworkWatcher::CServerManagerNetworkWatcher(CServerManager*)
- sym.CServerManager::BStart
- sym.CServerManager::Update
  - CDiscoverySocket::BReceive(netadr_t*,CUtlBuffer*)
  - CServerManager::BProcess(netadr_t*,CUtlBuffer*)
  - CServerManager::RequestStatus()
  - CServerManager::SendStartStreamingRequest()
  -