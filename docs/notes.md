## TODO
- Add filtering for msg_type (maybe even advanced filtering later)
- Add in some sort of fake REPL for sending forged packets (go all out with Lua/Gluon REPL?)
- How does the device proof request challenge and response work?
- How is the device authorization request encrypted?

## Notes
- Rust macros are a pain to debug... `cargo +nightly rustc --all-features --lib -- -Z unstable-options --pretty=expanded | less`
- CMsgRemoteClientBroadcastHeader::client_id's are stored in ~/.local/share/Steam/userdata/sid/config/localconfig.vdf under "streaming"{ "Devices"{} }
- UDP
  - 27017-27018 - [Steam Friends](https://web.archive.org/web/20160712124341/https://imfreedom.org/wiki/Steam_Friends)
  - 27031 - IHS Stream Data - First 9 bytes seem to stay the same (Likely protobuf video/input data)
  - 27036 - Steam Discovery/Control Protocol (Android/SteamLink) - https://fossies.org/linux/wireshark/epan/dissectors/packet-steam-ihs-discovery.c
- TCP
  - 27036 - Steam2Steam Control Protocol - TLSv1.2 TLS_PSK_WITH_AES_128_CBC_SHA - Negotiation done prior to game launch

## Message Type Map
| Type                          | ERemoteClientBroadcastMsg Enum               | Body Message                          | Description                                     |
| ----------------------------- | -----------------------------------          | ------------------------------------- | ----------------------------------------------- |
| Client Discovery              | KERemoteClientBroadcastMsgDiscovery          | CMsgRemoteClientBroadcastDiscovery    | Discovers IHS servers on networks |
| Client Status                 | KERemoteClientBroadcastMsgStatus             | CMsgRemoteClientBroadcastStatus       | Sent in response to discovery, also sent randomly as a broadcast by servers |
| Client Offline                | KERemoteClientBroadcastMsgOffline            | None                                  | Marks a client as no-longer available |
| Client ID Deconflict          | KERemoteClientBroadcastMsgClientIdDeconflict | CMsgRemoteClientBroadcastClientIdDeconflict | I'm guessing this asks to re-create client UUIDs? |
| Device Streaming Request      | KERemoteDeviceStreamingRequest               | CMsgRemoteDeviceStreamingRequest      | Sent client->server to request the initial connection and the authenticated session with client information |
| Device Streaming Response     | KERemoteDeviceStreamingResponse              | CMsgRemoteDeviceStreamingResponse     | Sent server->client with an auth result; success contains the session enc. key and stream port [See ERemoteDeviceStreamingResult] |
| Device Streaming Cancel Req.  | KERemoteDeviceStreamingCancelRequest         | CMsgRemoteDeviceStreamingCancelRequest| |
| Device Proof Request          | KERemoteDeviceProofRequest                   | CMsgRemoteDeviceProofRequest          | Sent server->client if client_id exists already, challenge the stored private key |
| Device Proof Response         | KERemoteDeviceProofResponse                  | CMsgRemoteDeviceProofResponse         | Sent client->server prove ownership of private key (response is much larger than the request) |
| Device Authorization Request  | KERemoteDeviceAuthorizationRequest           | CMsgRemoteDeviceAuthorizationRequest  | Sent client->server with a token, device name, and encrypted request (OTP pin is done here) |
| Device Authorization Response | KERemoteDeviceAuthorizationResponse          | CMsgRemoteDeviceAuthorizationResponse | Sent server->client [See ERemoteDeviceAuthorizationResult] |
| Device Auth. Cancel Request   | KERemoteDeviceAuthorizationCancelRequest     | CMsgRemoteDeviceAuthorizationCancelRequest | |


## Useful Links
- [Discovery Protocol](https://web.archive.org/web/20181123002511/https://codingrange.com/blog/steam-in-home-streaming-discovery-protocol)
- [Wireshark Discovery](https://fossies.org/linux/wireshark/epan/dissectors/packet-steam-ihs-discovery.c)
- [Control Protocol](https://web.archive.org/web/20181123002337/https://codingrange.com/blog/steam-in-home-streaming-control-protocol)
