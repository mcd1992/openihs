import r2pipe
import pprint

# Print breakpoints for all CCrypto methods

pp = pprint.PrettyPrinter(indent=4)
r2=r2pipe.open()  # open without arguments only for #!pipe

for sym in r2.cmdj('isj'):
    #pp.pprint(sym)
    if 'demname' in sym:
        if 'CCrypto::' in sym.get('demname'):
            print("# {}".format(sym.get('demname')))
            print("break *0x{:x}\n".format(sym.get('vaddr')))
