import r2pipe
import pprint
import re

# Demangle symbol from r2 PLT/subroutine name into the comments

pp = pprint.PrettyPrinter(indent=4)
symregex = r'call sub\.(_Z.*?)_[a-f0-9]+_[a-f0-9]+'
r2=r2pipe.open()  # open without arguments only for #!pipe

for op in r2.cmdj('pdfj @ $$')['ops']:
    if (not op.get('comment') and 'call sub._Z' in op.get('disasm')):
        #pp.pprint(op)
        res = re.search(symregex, op.get('disasm'))
        if (res and res.group(1)):
            sym = r2.cmd('iD c++ ' + res.group(1)).rstrip("\n\r")
            if (sym):
                r2.cmd(str.format("\"CC {0:s}\" @ 0x{1:x}", sym, op.get('offset')))
