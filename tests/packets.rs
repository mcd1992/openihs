use openihs::packets;
use openihs::protos::discovery;
use openihs::packets::IHSPacket;
use bytes::{BytesMut, BufMut};

#[test]
fn test_discovery_packet() {
    // Test decode from raw bytes
    let mut decode_me = BytesMut::new();
    decode_me.put_slice(b"\xff\xff\xff\xff\x21\x4c\x5f\xa0\x0c\x00\x00\x00\x08\xe3\xcf\x80\x80\xb0\xfd\x80\xa6\x53\x10\x00\x02\x00\x00\x00\x08\x07");
    let decoded_pkt = packets::discovery::Discovery::decode(decode_me);
    assert!(decoded_pkt.is_ok());

    // Test create a new IHSPacket from defaults
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgDiscovery);

    let mut body = discovery::CMsgRemoteClientBroadcastDiscovery::new();
    body.set_seq_num(10);
    pkt.body = Some(Box::new(body));

    // Test encode the decoded packet and created packet from above
    let mut buf = bytes::BytesMut::new();
    let mut err = decoded_pkt.unwrap().encode(&mut buf);
    assert!(err.is_ok(), err);
    err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_status_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgStatus);
    pkt.header.set_instance_id(17828145480261643969);

    let mut body = discovery::CMsgRemoteClientBroadcastStatus::new();
    body.set_version(8);
    body.set_min_version(6);
    body.set_connect_port(27036);
    body.set_hostname(String::from("openihstest"));
    body.set_enabled_services(0x02);
    body.set_ostype(0b10000);
    body.set_is64bit(true);

    let mut user = discovery::CMsgRemoteClientBroadcastStatus_User::new();
    user.set_steamid(76561197991350071);
    user.set_auth_key_id(12345);
    body.set_users(protobuf::RepeatedField::from_vec(vec![user]));

    body.set_euniverse(1);
    body.set_timestamp(1554909304);
    body.set_games_running(false);
    body.set_mac_addresses(protobuf::RepeatedField::from_vec(vec![
        String::from("52-54-00-DE-AD-69"),
        String::from("52-54-00-BE-EF-69"),
    ]));
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_authrequest_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceAuthorizationRequest);

    let mut body = discovery::CMsgRemoteDeviceAuthorizationRequest::new();
    body.set_device_token(vec![0x69; 32]);
    body.set_device_name(String::from("openihstest"));
    body.set_encrypted_request(vec![0x69; 256]);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_authresponse_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceAuthorizationResponse);

    let mut body = discovery::CMsgRemoteDeviceAuthorizationResponse::new();
    body.set_result(discovery::ERemoteDeviceAuthorizationResult::k_ERemoteDeviceAuthorizationSuccess);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_streamrequest_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceStreamingRequest);

    let mut body = discovery::CMsgRemoteDeviceStreamingRequest::new();
    body.set_request_id(69);
    body.set_maximum_resolution_x(1920);
    body.set_maximum_resolution_y(1080);
    body.set_audio_channel_count(2);
    body.set_device_version(String::from("openihstest"));
    body.set_stream_desktop(true);
    body.set_device_token(vec![0x69; 32]);
    body.set_pin(vec![5, 4, 3, 2, 1]);
    body.set_network_test(false);

    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_streamresponse_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceStreamingResponse);

    let mut body = discovery::CMsgRemoteDeviceStreamingResponse::new();
    body.set_request_id(69);
    body.set_port(27031);
    body.set_encrypted_session_key(vec![0x69; 48]);

    body.set_result(discovery::ERemoteDeviceStreamingResult::k_ERemoteDeviceStreamingSuccess);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_proofrequest_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceProofRequest);

    let mut body = discovery::CMsgRemoteDeviceProofRequest::new();
    body.set_challenge(vec![0x69; 16]);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_proofresponse_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceProofResponse);

    let mut body = discovery::CMsgRemoteDeviceProofResponse::new();
    body.set_response(vec![0x69; 48]);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_authcancelrequest_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceAuthorizationCancelRequest);

    pkt.body = Some(Box::new(discovery::CMsgRemoteDeviceAuthorizationCancelRequest::new()));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_streamcancelrequest_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceStreamingCancelRequest);

    let mut body = discovery::CMsgRemoteDeviceStreamingCancelRequest::new();
    body.set_request_id(69);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}

#[test]
fn test_clientiddeconflict_packet() {
    let mut pkt: packets::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(6001415647646095869);
    pkt.header
        .set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgClientIDDeconflict);

    let mut body = discovery::CMsgRemoteClientBroadcastClientIDDeconflict::new();
    body.set_client_ids(vec![6001415647646095869]);
    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let err = pkt.encode(&mut buf);
    assert!(err.is_ok(), err);
}
