//! Defines an encode/decode-able struct that corresponds to IHS data packets
//! Also defines some reusable en/decryption methods used by Valve

pub mod discovery;
pub mod enummap;
pub mod stream;

const NULL_IV: [u8; 16] = [0; 16]; // Used in the symmetric crypto methods
const RSA_PUBKEY: &str = "\
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAx7iQLqQkm17hJPpGiOpb/QJHtQpi\
Kk2pm1zomiuqs3S4bwOroZ+mX9yGys5GpE61q0IaadEDeMO4ZyPfwjlb6kqznauRJ2Hj40Or\
mCSwRW5X2rre/aTRwcerKF5kfbEYi8z47+WKdZXM3TGEXAz3W4q0snB9IY721GoRXQhfzHVK\
dcUN2NAYk2z16WQsZkVjfsO1CUgRuc7tsX0jR4HOVEXC3bOKeHyVhRIWfXovPzR5rCchqt2Y\
EZ7oMvMhxinGVyQ0okL1r/UTB87aCCMu9WRg1PGRyPSISj4qFDYXEAxDz1WtH8fHBSB9hoaX\
zJsj3u1GB2MvfqVNSYmDPckg5wIBEQ==";

pub trait IHSPacket: std::fmt::Debug {
    // Takes a struct and 'encodes' it for transmission over-the-wire
    fn encode(&self, data: &mut bytes::BytesMut) -> Result<(), &'static str>;

    // Takes raw bytes and creates a new struct
    fn decode(data: &bytes::BytesMut) -> Result<Self, &'static str>
    where
        Self: Sized;
}

pub fn rsa_encrypt(data: &Vec<u8>) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    let pubkey_der = base64::decode(RSA_PUBKEY)?;
    let pubkey = openssl::rsa::Rsa::public_key_from_der(pubkey_der.as_slice())?;
    let mut ciphertext = vec![0; pubkey.size() as usize];
    let datalen = pubkey.public_encrypt(data.as_slice(), &mut ciphertext, openssl::rsa::Padding::PKCS1_OAEP)?;
    ciphertext.truncate(datalen);
    Ok(ciphertext)
}

// Valve uses AES_256_CBC where the IV is randomly generated and encrypted with the key but no IV itself
// Then the actual payload is encrypted with the key and the random IV at the start of the ciphertext
// So their encrypted payloads will start with 16 bytes of encrypted IV then the rest is the data ciphertext
pub fn symmetric_encrypt_random_iv(data: &Vec<u8>, aes_key: &Vec<u8>) -> Result<Vec<u8>, openssl::error::ErrorStack> {
    let block_size = openssl::symm::Cipher::aes_256_cbc().block_size();
    let mut ciphertext = vec![0u8; 16 + data.len() + block_size]; // IV ciphertext + Data + block align
    let mut cipherlen = 0;

    let mut rand_iv = [0; 16];
    openssl::rand::rand_bytes(&mut rand_iv)?;

    // AES encrypt the IV with no IV itself
    let mut iv_crypter = openssl::symm::Crypter::new(
        openssl::symm::Cipher::aes_256_cbc(),
        openssl::symm::Mode::Encrypt,
        &aes_key,
        Some(&NULL_IV),
    )?;
    iv_crypter.pad(false); // The IV is always 16 bytes
    cipherlen += iv_crypter.update(&rand_iv, &mut ciphertext)?;
    cipherlen += iv_crypter.finalize(&mut ciphertext[cipherlen..])?;

    // Encrypt the data with the random IV generated above
    let mut data_crypter = openssl::symm::Crypter::new(
        openssl::symm::Cipher::aes_256_cbc(),
        openssl::symm::Mode::Encrypt,
        &aes_key,
        Some(&rand_iv),
    )?;
    cipherlen += data_crypter.update(data, &mut ciphertext[cipherlen..])?;
    cipherlen += data_crypter.finalize(&mut ciphertext[cipherlen..])?;

    ciphertext.truncate(cipherlen);
    Ok(ciphertext)
}

pub fn symmetric_decrypt_recover_iv(ciphertext: &Vec<u8>, aes_key: &Vec<u8>) -> Result<Vec<u8>, openssl::error::ErrorStack> {
    assert!(ciphertext.len() > 31);
    let block_size = openssl::symm::Cipher::aes_256_cbc().block_size();

    // AES decrypt the IV with no IV itself
    let mut rand_iv = vec![0u8; 16 + block_size];
    let mut iv_len = 0;
    let mut iv_crypter = openssl::symm::Crypter::new(
        openssl::symm::Cipher::aes_256_cbc(),
        openssl::symm::Mode::Decrypt,
        &aes_key,
        Some(&NULL_IV),
    )?;
    iv_crypter.pad(false); // The IV is always 16 bytes
    iv_len += iv_crypter.update(&ciphertext[..16], &mut rand_iv)?;
    iv_len += iv_crypter.finalize(&mut rand_iv[iv_len..])?;
    rand_iv.truncate(iv_len);

    // AES decrypt the data with the above IV
    let mut data = vec![0u8; ciphertext.len()];
    let mut datalen = 0;
    let mut data_crypter = openssl::symm::Crypter::new(
        openssl::symm::Cipher::aes_256_cbc(),
        openssl::symm::Mode::Decrypt,
        &aes_key,
        Some(&rand_iv),
    )?;
    datalen += data_crypter.update(&ciphertext[16..], &mut data)?;
    datalen += data_crypter.finalize(&mut data[datalen..])?;

    data.truncate(datalen);
    Ok(data)
}

// Used by the stream control packets
pub fn symmetric_decrypt_hmac(ciphertext: &Vec<u8>, session_key: &Vec<u8>) -> Result<Vec<u8>, openssl::error::ErrorStack> {
    assert!(ciphertext.len() > 31);
    let iv_hmac = ciphertext[..16].to_vec();

    // AES decrypt the data with the above IV(HMAC)
    let mut data = vec![0u8; ciphertext.len()];
    let mut datalen = 0;
    let mut data_crypter = openssl::symm::Crypter::new(
        openssl::symm::Cipher::aes_256_cbc(),
        openssl::symm::Mode::Decrypt,
        &session_key,
        Some(&iv_hmac),
    )?;
    datalen += data_crypter.update(&ciphertext[16..], &mut data)?;
    datalen += data_crypter.finalize(&mut data[datalen..])?;
    data.truncate(datalen);

    // Calculate and check HMAC
    let hmac_key = openssl::pkey::PKey::hmac(&session_key)?;
    let mut signer = openssl::sign::Signer::new(openssl::hash::MessageDigest::md5(), &hmac_key)?;
    signer.update(&data)?;
    let calculated_hmac = signer.sign_to_vec()?;
    assert!(openssl::memcmp::eq(&iv_hmac, &calculated_hmac));

    Ok(data)
}