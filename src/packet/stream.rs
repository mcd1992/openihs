#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use crate::packet::IHSPacket;
use crate::packet::enummap::decode_stream_control;
use crate::proto::{stream, stream::EStreamChannel, stream::EStreamStatsMessage};
use std::convert::TryFrom;
use crc32c::crc32c;
use protobuf::ProtobufEnum;
use nom::HexDisplay;

// Thanks go to https://github.com/krzys-h/SteamStreaming

#[derive(Debug)]
pub enum TransportType {
    // https://github.com/krzys-h/SteamStreaming/blob/master/SteamStreaming/Protocols/Transport/SteamStreamTransport.cs#L385
    Invalid             = -1,
    Raw                 = 0,  // Fragmentation and acknowledgement are disabled, no sequence numbers. Used only during MTU probing.
    Syn                 = 1,  // Request to start connection (for transport layer use only)
    SynAck              = 2,  // Acknowledge connection start (for transport layer use only)
    Unreliable          = 3,  // Packets that don't require acknowledgement
    UnreliableFragment  = 4,  // If an Unreliable packet is fragmented, the following packets get this type
    Reliable            = 5,  // Packets that require ackowledgement
    ReliableFragment    = 6,  // If a Reliable packet is fragmented, the following packets get this type
    ReliableACK         = 7,  // Ackowledgement for Reliable packets
    Unused              = 8,  // Not sure
    Fin                 = 9,  // Request and acknowledge connection end (for transport layer use only)
    Unknown             = 10, // Not sure what this is used for yet (Also seems to use channel4/unknown)
}

impl TransportType {
    pub fn from_u8(discriminant: u8) -> Self {
        match discriminant {
            x if x == TransportType::Raw as u8 => TransportType::Raw,
            x if x == TransportType::Syn as u8 => TransportType::Syn,
            x if x == TransportType::SynAck as u8 => TransportType::SynAck,
            x if x == TransportType::Unreliable as u8 => TransportType::Unreliable,
            x if x == TransportType::UnreliableFragment as u8 => TransportType::UnreliableFragment,
            x if x == TransportType::Reliable as u8 => TransportType::Reliable,
            x if x == TransportType::ReliableFragment as u8 => TransportType::ReliableFragment,
            x if x == TransportType::ReliableACK as u8 => TransportType::ReliableACK,
            x if x == TransportType::Unused as u8 => TransportType::Unused,
            x if x == TransportType::Fin as u8 => TransportType::Fin,
            x if x == TransportType::Unknown as u8 => TransportType::Unknown,
            _ => TransportType::Invalid,
        }
    }
}

#[derive(Debug)]
pub enum BodyType {
    None,
    Error,
    Raw(Vec<u8>), // TODO: remove when we have defined/figured out all body types
    AckTimestamp(u32),

    // Called Discovery in stream.proto, only seems to be used for MTU though
    MTU {
        msg_type: stream::EStreamDiscoveryMessage, // (1=request, 2=response)
        mtu_type: MTUType, // (1=?, 2=?, 3=?, 4=ping, 5=payload)
        //always8: u8, // always seems to be 0x08, asserts otherwise
        seq_num: u8,
        unknown: u8, // Seems to always be 0x10 or 0x01, not sure what its for yet
        payload: Option<Vec<u8>>,
    },

    // Protobuf-using bodies
    Control {
        msg_type: stream::EStreamControlMessage,
        protobuf: Option<Box<dyn protobuf::Message>>,
        ciphertext: Option<Vec<u8>>, // Debug type for encrypted bodies
    },
    Data {
        msg_type: stream::EStreamDataMessage,
        protobuf: Option<Box<dyn protobuf::Message>>,
        ciphertext: Option<Vec<u8>>, // Debug type for encrypted bodies
    },
    Stats {
        msg_type: stream::EStreamStatsMessage,
        protobuf: Option<Box<dyn protobuf::Message>>,
        ciphertext: Option<Vec<u8>>, // Debug type for encrypted bodies
    },
}

#[derive(Debug)]
pub enum MTUType {
    Ping = 4,
    Payload = 5,
}


#[derive(Debug)]
pub struct Stream {
    pub has_checksum: bool,            // is there a CRC checksum
    pub transport_type: TransportType, // type of transport, see TransportType enum
    pub retry_count: u8,               // increments for dropped packets
    pub sender_id: u8,                 // sender id
    pub receiver_id: u8,               // receiver id
    pub channel: EStreamChannel,       // stream channel (see proto::stream::EStreamChannel)
    pub fragment: u16,                 // reconstructing split packets
    pub sequence: u16,                 // sequence id
    pub timestamp: u32,                // timestamp ?
    pub body: BodyType,                // optional body
    pub crc32: Option<u32>,            // CRC checksum
}

impl IHSPacket for Stream {
    fn encode(&self, _buf: &mut bytes::BytesMut) -> Result<(), &'static str> {
        Ok(())
    }

    fn decode(data: &bytes::BytesMut) -> Result<Stream, &'static str> {
        if data.len() < 14 {
            return Err("Failed to parse data as stream packet: Not enough data");
        }

        let has_checksum = (data[0] >> 7) != 0;
        let transport_type_byte = u8::from_le_bytes([data[0] & 0b01111111]);
        let transport_type = TransportType::from_u8(transport_type_byte);

        let retry_count = u8::from_le_bytes([data[1]]);
        let sender_id = u8::from_le_bytes([data[2]]);
        let receiver_id = u8::from_le_bytes([data[3]]);
        let channel_byte = u8::from_le_bytes([data[4]]);
        let channel = EStreamChannel::from_i32(channel_byte as i32).unwrap_or(EStreamChannel::k_EStreamChannelInvalid);

        let fragment = u16::from_le_bytes(<[u8; 2]>::try_from(&data[5..7]).unwrap());
        let sequence = u16::from_le_bytes(<[u8; 2]>::try_from(&data[7..9]).unwrap());
        let timestamp = u32::from_le_bytes(<[u8; 4]>::try_from(&data[9..13]).unwrap());

        let payload: Vec<u8>;
        let mut crc32 = None;
        if has_checksum {
            let len = data.len();
            let pkt_crc = u32::from_le_bytes(<[u8; 4]>::try_from(&data[len - 4..]).unwrap());
            let calc_crc = crc32c(&data[..len - 4]);
            if pkt_crc != calc_crc {
                return Err("CRC32C Mismatch");
            }
            crc32 = Some(pkt_crc);
            payload = data[13..len - 4].to_vec();
        } else {
            payload = data[13..].to_vec();
        }

        let mut body = BodyType::None;
        match transport_type {
            TransportType::Raw => {
                match channel {
                    EStreamChannel::k_EStreamChannelDiscovery => {
                        // Used for MTU 'negotiation'
                        // msg_type: u8 = EStreamDiscoveryMessage
                        // payload = raw bytes for MTU negotiation
                        // NOTE: sender/receiver_id are 0x00
                        let msg_type = stream::EStreamDiscoveryMessage::from_i32(payload[0] as i32).unwrap();
                        let mtu_type = match u32::from_le_bytes(<[u8; 4]>::try_from(&payload[1..=4]).unwrap()) {
                            x if x == MTUType::Ping as u32 => MTUType::Ping,
                            x if x == MTUType::Payload as u32 => MTUType::Payload,
                            x => unimplemented!("Unknown MTUType: {:#?}", x),
                        };
                        assert!(u8::from_le_bytes([payload[5]]) == 0x08); // Seems to always be 0x08, lets find out
                        let seq_num = u8::from_le_bytes([payload[6]]);
                        let unknown = u8::from_le_bytes([payload[7]]); // Seems to always be 0x10 or 0x01
                        //let payload_size = u16::from_le_bytes(<[u8; 2]>::try_from(&payload[8..=9]).unwrap());
                        let _payload = Some(payload[8..].to_vec());

                        body = BodyType::MTU {
                            msg_type,
                            mtu_type,
                            seq_num,
                            unknown,
                            payload: None, // TODO: Remove. Temporarily commented out to clean up debug output
                        };
                    }
                    // NOTE: Seems pretty safe that this is only used for MTU on discovery channel
                    c => unimplemented!("TransportType::Raw channel: {:#?}", c),
                }
            }
            TransportType::Syn => {
                match channel {
                    EStreamChannel::k_EStreamChannelDiscovery => {
                        // Initial connection client -> server
                        // payload = magic number?
                        body = BodyType::Raw(payload);
                    }
                    // NOTE: Seems to only be used with discovery channel for initial handshake
                    c => unimplemented!("TransportType::Syn channel: {:#?}", c),
                }
            }
            TransportType::SynAck => {
                match channel {
                    EStreamChannel::k_EStreamChannelDiscovery => {
                        // Ack connection server -> client
                        // payload = ack timestamp
                        let ts = u32::from_le_bytes(<[u8; 4]>::try_from(&payload[..4]).unwrap());
                        body = BodyType::AckTimestamp(ts);
                    }
                    // NOTE: Seems to only be used with discovery channel for initial handshake ack
                    c => unimplemented!("TransportType::SynAck channel: {:#?}", c),
                }
            }
            TransportType::Unreliable => {
                match channel {
                    EStreamChannel::k_EStreamChannelDataChannelStart => {
                        // u8 msg_type = EStreamDataMessage?
                        // u8 seq_num again?
                        // TODO: dig into this more
                        // Something is very weird with the channel as well
                        body = BodyType::Raw(payload);
                    }
                    EStreamChannel::k_EStreamChannelUnknown => {
                        // Not sure what this is yet
                        // Does have a lot of fragmented data as well
                        // Maybe the actual video data? I'm seeing VLC strings in it
                        body = BodyType::Raw(payload);
                    }
                    c => unimplemented!("TransportType::Unreliable channel: {:#?}", c),
                }
            }
            TransportType::UnreliableFragment => {
                //unimplemented!("UnreliableFragment");
            }
            TransportType::Reliable => {
                match channel {
                    EStreamChannel::k_EStreamChannelControl => {
                        // Control data can be client <-> server bidirectional
                        // Contains protobuf data related to msg_type after timestamp
                        // u8 msg_type
                        // protobuf data
                        let msg_type = stream::EStreamControlMessage::from_i32(payload[0] as i32).unwrap();

                        match msg_type { // These seem to be the only unencrypted message types?
                            stream::EStreamControlMessage::k_EStreamControlAuthenticationRequest => (),
                            stream::EStreamControlMessage::k_EStreamControlAuthenticationResponse => (),
                            stream::EStreamControlMessage::k_EStreamControlClientHandshake => (),
                            stream::EStreamControlMessage::k_EStreamControlServerHandshake => (),
                            _ => (),
                        }

                        match msg_type {
                            // TODO: Decrypt
                            // These messages seem to be encrypted, or maybe EVERYTHING after the above types is encrypted?
                            // These message types are just what I grepped out in my example pcaps, theres likely more
                            // Just putting this here temporarily so I can have it in git history, likely wont need this
                            stream::EStreamControlMessage::k_EStreamControlGetCursorImage => (),
                            stream::EStreamControlMessage::k_EStreamControlHideCursor => (),
                            stream::EStreamControlMessage::k_EStreamControlNegotiationComplete => (),
                            stream::EStreamControlMessage::k_EStreamControlNegotiationInit => (),
                            stream::EStreamControlMessage::k_EStreamControlNegotiationSetConfig => (),
                            stream::EStreamControlMessage::k_EStreamControlRemoteHID => (),
                            stream::EStreamControlMessage::k_EStreamControlSetActivity => (),
                            stream::EStreamControlMessage::k_EStreamControlSetCaptureSize => (),
                            stream::EStreamControlMessage::k_EStreamControlSetCursor => (),
                            stream::EStreamControlMessage::k_EStreamControlSetCursorImage => (),
                            stream::EStreamControlMessage::k_EStreamControlSetIcon => (),
                            stream::EStreamControlMessage::k_EStreamControlSetQoS => (),
                            stream::EStreamControlMessage::k_EStreamControlSetSpectatorMode => (),
                            stream::EStreamControlMessage::k_EStreamControlSetTargetBitrate => (),
                            stream::EStreamControlMessage::k_EStreamControlSetTargetFramerate => (),
                            stream::EStreamControlMessage::k_EStreamControlSetTitle => (),
                            stream::EStreamControlMessage::k_EStreamControlStartAudioData => (),
                            stream::EStreamControlMessage::k_EStreamControlStartVideoData => (),
                            stream::EStreamControlMessage::k_EStreamControlVideoDecoderInfo => (),
                            stream::EStreamControlMessage::k_EStreamControlVideoEncoderInfo => (),
                            _ => (),
                        }

                        let protobuf = decode_stream_control(msg_type, &payload[1..]);
                        if protobuf.is_none() {
                            debug!("Failed to decode reliable control packet, maybe encrypted: {:#?}", msg_type);
                        }
                        body = BodyType::Control {
                            msg_type,
                            protobuf,
                            ciphertext: None,
                        }
                    }
                    EStreamChannel::k_EStreamChannelStats => {
                        let msg_type = stream::EStreamStatsMessage::from_i32(payload[0] as i32).unwrap();
                        // See EStreamStatsMessage
                        // u8 msg_type
                        // protobuf data
                        match msg_type {
                            EStreamStatsMessage::k_EStreamStatsFrameEvents => {
                                let p = protobuf::parse_from_bytes::<stream::CFrameStatsListMsg>(&payload[1..]);
                                if p.is_ok() {
                                    body = BodyType::Stats {
                                        msg_type,
                                        protobuf: Some(Box::new(p.unwrap())),
                                        ciphertext: None,
                                    }
                                } else {
                                    body = BodyType::Error;
                                }
                            }
                            EStreamStatsMessage::k_EStreamStatsDebugDump => {
                                let p = protobuf::parse_from_bytes::<stream::CDebugDumpMsg>(&payload[1..]);
                                if p.is_ok() {
                                    body = BodyType::Stats {
                                        msg_type,
                                        protobuf: Some(Box::new(p.unwrap())),
                                        ciphertext: None,
                                    }
                                } else {
                                    body = BodyType::Error;
                                }
                            }
                            EStreamStatsMessage::k_EStreamStatsLogMessage => {
                                let p = protobuf::parse_from_bytes::<stream::CLogMsg>(&payload[1..]);
                                if p.is_ok() {
                                    body = BodyType::Stats {
                                        msg_type,
                                        protobuf: Some(Box::new(p.unwrap())),
                                        ciphertext: None,
                                    }
                                } else {
                                    body = BodyType::Error;
                                }
                            }
                            _ => {
                                // Assume CLogUploadMsg since the last 3 reuse this same protobuf
                                let p = protobuf::parse_from_bytes::<stream::CLogUploadMsg>(&payload[1..]);
                                if p.is_ok() {
                                    body = BodyType::Stats {
                                        msg_type,
                                        protobuf: Some(Box::new(p.unwrap())),
                                        ciphertext: None,
                                    }
                                } else {
                                    body = BodyType::Error;
                                }
                            }
                        }
                    }
                    c => unimplemented!("TransportType::Reliable channel: {:#?}", c),
                }
            }
            TransportType::ReliableFragment => {
                //unimplemented!("ReliableFragment");
            }
            TransportType::ReliableACK => {
                let ts = u32::from_le_bytes(<[u8; 4]>::try_from(&payload[..4]).unwrap());
                body = BodyType::AckTimestamp(ts);
                match channel {
                    EStreamChannel::k_EStreamChannelControl => {
                        // Ack reliable data from client
                        // payload = ack timestamp
                        // TODO: wtf the ack timestamp doesn't always match the previous timestamp[s]?
                    }
                    EStreamChannel::k_EStreamChannelStats => {
                        // Should be the same for all channels?
                    }
                    c => unimplemented!("TransportType::ReliableACK channel: {:#?}", c),
                }
            }
            TransportType::Unused => {
                unimplemented!("{}", format!("Unused transport type found: \n{}", data.to_hex(16)));
            }
            TransportType::Fin => {
                match channel {
                    EStreamChannel::k_EStreamChannelDiscovery => {
                        // Seems to contain no real data, just its existance means stream end
                        // The src/dest does matter since they reuse this as an 'ACK' as well
                        body = BodyType::Raw(payload);
                    }
                    c => unimplemented!("TransportType::Fin channel: {:#?}", c),
                }
            }
            TransportType::Unknown => {
                // Seems to be related to FPS / Frame drops? (log messages seem to exist before it)
                // contains no data after the timestamp (still has a checksum at end)
            }
            TransportType::Invalid => {
                panic!("{}", format!("Invalid transport type in stream: \n{}", data.to_hex(16)));
            }
        }

        return Ok(Stream {
            has_checksum,
            transport_type,
            retry_count,
            sender_id,
            receiver_id,
            channel,
            fragment,
            sequence,
            timestamp,
            body,
            crc32,
        });
    }
}

impl Default for Stream {
    fn default() -> Stream {
        Stream {
            has_checksum: false,
            transport_type: TransportType::Invalid,
            retry_count: 0,
            sender_id: 0,
            receiver_id: 0,
            channel: EStreamChannel::k_EStreamChannelInvalid,
            fragment: 0,
            sequence: 0,
            timestamp: 0,
            body: BodyType::None,
            crc32: None,
        }
    }
}
