use crate::proto::discovery;
use crate::packet::IHSPacket;
use bytes::BufMut;
use protobuf::Message;

#[derive(Debug)]
pub struct Discovery {
    pub signature: u64, // Constant signature used by STEAMDISCOVER 0xa05f4c21ffffffff
    //pub header_len: u32, // Size of the header protobuf object

    // Protobuf header
    pub header: discovery::CMsgRemoteClientBroadcastHeader,
    // client_id
    // msg_type: ERemoteClientBroadcastMsg
    // instance_id

    // Protobuf body: exists depending on header.msg_type
    //pub body_len: Option<u32>,
    pub body: Option<Box<dyn protobuf::Message>>,
}

impl IHSPacket for Discovery {
    fn encode(&self, buf: &mut bytes::BytesMut) -> Result<(), &'static str> {
        //buf.clear();
        buf.reserve(65507); // TODO: Perftest; Just reserve enough data for a full UDP/IP packet
        buf.put_u64_le(self.signature);

        let header_len = self.header.compute_size();
        buf.put_u32_le(header_len);

        // Header protobuf
        let header_bytes = self.header.write_to_bytes().expect("Failed to encode discovery packet.");
        buf.put_slice(header_bytes.as_slice());

        // Optional body
        if self.body.is_some() {
            let bodyref = &**(self.body.as_ref().unwrap());
            let size = bodyref.compute_size();
            let bytes = bodyref.write_to_bytes();
            if bytes.is_ok() {
                buf.put_u32_le(size);
                buf.put_slice(bytes.unwrap().as_slice());
            }
        }

        Ok(())
    }

    fn decode(data: &bytes::BytesMut) -> Result<Discovery, &'static str> {
        // Parse IHS signature
        if data.len() < 12 {
            return Err("Failed to parse data as discovery packet: Not enough data");
        }
        let mut signature_slice = [0; 8];
        signature_slice.copy_from_slice(&data[0..8]);
        let signature = u64::from_le_bytes(signature_slice);
        if signature != 0xa05f4c21ffffffff {
            return Err("Failed to parse data as discovery packet: Invalid signature");
        }

        // Parse IHS protobuf header
        let mut header_len_slice = [0; 4];
        header_len_slice.copy_from_slice(&data[8..12]);
        let header_len = u32::from_le_bytes(header_len_slice);
        if header_len == 0 {
            return Err("Failed to parse data as discovery packet: header_len == 0");
        }
        let header_end: usize = 12 + header_len as usize;
        if header_end > data.len() {
            return Err("Failed to parse data as discovery packet: header_end > data.len()");
        }
        let header = protobuf::parse_from_bytes::<discovery::CMsgRemoteClientBroadcastHeader>(&data[12..header_end]);
        if header.is_err() {
            return Err("Failed to parse data as discovery packet: Invalid header");
        }
        let header = header.unwrap(); // Is shadowing like this a sin in rust?

        // Parse IHS protobuf body
        let body = parse_body(&header, &data[header_end..]);
        if body.is_some() {
            return Ok(Discovery {
                signature,
                //header_len,
                header,
                //body_len: Some(body_len),
                body: Some(body.unwrap()),
            });
        }

        // Packet type has no body
        Ok(Discovery {
            signature,
            //header_len,
            header,
            //body_len: None,
            body: None,
        })
    }
}

impl Default for Discovery {
    fn default() -> Discovery {
        Discovery {
            signature: 0xa05f4c21ffffffff,
            //header_len: 0,
            header: Default::default(),
            //body_len: None,
            body: None,
        }
    }
}

// Parse the given data as header.msg_type
fn parse_body(header: &discovery::CMsgRemoteClientBroadcastHeader, data: &[u8]) -> Option<Box<dyn protobuf::Message>> {
    if data.len() < 4 {
        return None;
    }
    let mut body_len_slice = [0; 4];
    body_len_slice.copy_from_slice(&data[0..4]);
    let body_len = u32::from_le_bytes(body_len_slice) as usize;
    if data.len() < body_len { // The header says the body is larger than the data we've been given
        return None;
    };
    let msg_type = header.get_msg_type();
    let body = crate::packet::enummap::decode_discovery(msg_type, &data[4..body_len+4]); // UDP padding?

    if body.is_some() {
        body
    } else {
        None
    }
}
