#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};
use enumflags2::BitFlags;

use openihs::client;
use openihs::client::events::ClientEvent;
use openihs::proto::discovery::{CMsgRemoteDeviceStreamingResponse, ERemoteDeviceStreamingResult};
use openihs::proto::discovery::{CMsgRemoteDeviceAuthorizationResponse, ERemoteDeviceAuthorizationResult};
use std::time::Duration;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

static EXIT_LOOP: AtomicBool = AtomicBool::new(false);

#[repr(u8)]
#[derive(BitFlags, Copy, Clone, Debug, PartialEq)]
enum WantState {
    Stream        = 0b0001,
    Authorization = 0b0010,
}

fn connect(_log_level: u64, ipaddr: &str) {
    ctrlc::set_handler(|| {
        println!("Exiting...");
        EXIT_LOOP.store(true, Ordering::Relaxed);
    }).expect("Error setting Ctrl-C handler");

    let cstate = client::new().expect("Failed to initialize ClientState");
    let _ = cstate.send_discovery();
    std::thread::sleep(std::time::Duration::new(1, 0)); // give the 'servers' time to respond

    let mut server = None;
    for (_id, info) in cstate.get_servers() {
        if info.ip_addresses.contains(&String::from(ipaddr)) {
            debug!("Sending initial stream request to {:#?}", info.ip_addresses[0]);
            if cstate.send_stream_request(&info).is_ok() {
                server = Some(info);
            }
        }
    }
    if server.is_none() {
        error!("Could not find server: {}", ipaddr);
    }


    let timer = crossbeam::channel::tick(Duration::new(3, 0));
    let mut want_map = BitFlags::from_flag(WantState::Stream);
    debug!("ENTERING CMD_CONNECT EVENT LOOP!");
    while server.is_some() && !EXIT_LOOP.load(Ordering::Relaxed) {
        if let Ok(_) = timer.try_recv() { // Send events 'timer'
            if want_map.contains(WantState::Stream) {
                debug!("      [SEND] CMsgRemoteDeviceStreamingRequest");
                let _ = cstate.send_stream_request(server.as_ref().unwrap());
            }
            if want_map.contains(WantState::Authorization) {
                debug!("      [SEND] CMsgRemoteDeviceAuthorizationRequest");
                let _ = cstate.send_authorization_request(server.as_ref().unwrap());
            }
        }

        // Receive events
        let event = cstate.event_recv_timeout(Duration::new(0, 250_000_000));
        let event = if event.is_ok() { event.unwrap() } else { ClientEvent::None };
        trace!("received event: {:#?}", event);
        match event {
            ClientEvent::StreamResponse(pkt) => {
                let body: &CMsgRemoteDeviceStreamingResponse = pkt.body.as_ref().unwrap().as_any().downcast_ref::<CMsgRemoteDeviceStreamingResponse>().unwrap();
                match body.get_result() {
                    ERemoteDeviceStreamingResult::k_ERemoteDeviceStreamingUnauthorized => {
                        debug!("[RECV] k_ERemoteDeviceStreamingUnauthorized");
                        want_map.insert(WantState::Authorization);
                    }
                    ERemoteDeviceStreamingResult::k_ERemoteDeviceStreamingSuccess => {
                        debug!("[RECV] k_ERemoteDeviceStreamingSuccess");
                        want_map.remove(WantState::Stream);
                    }
                    res => {
                        debug!("[RECV] {:#?}", res);
                    },
                }
            }
            ClientEvent::AuthorizationResponse(pkt) => {
                let body: &CMsgRemoteDeviceAuthorizationResponse = pkt.body.as_ref().unwrap().as_any().downcast_ref::<CMsgRemoteDeviceAuthorizationResponse>().unwrap();
                match body.get_result() {
                    ERemoteDeviceAuthorizationResult::k_ERemoteDeviceAuthorizationSuccess => {
                        debug!("[RECV] k_ERemoteDeviceAuthorizationSuccess");
                        want_map.remove(WantState::Authorization);
                    }
                    res => {
                        debug!("[RECV] {:#?}", res);
                    }
                }
            }
            ClientEvent::DeviceProofRequest(pkt) => {
                debug!("[RECV] CMsgRemoteDeviceProofRequest");
                //let body: &CMsgRemoteDeviceProofRequest = pkt.body.as_ref().unwrap().as_any().downcast_ref::<CMsgRemoteDeviceProofRequest>().unwrap();
                let _ = cstate.send_proof_response(server.as_ref().unwrap(), pkt);
                debug!("      [SEND] CMsgRemoteDeviceProofResponse");
            }
            _ => (),
        }
    }

    cstate.shutdown();
}

pub fn subcommand_entry_connect(_clap_matches: &clap::ArgMatches) {
    let connect_subcmd = _clap_matches.subcommand_matches("connect").expect("missing subcommand: connect");
    let ipaddr = connect_subcmd.value_of("ipaddr").expect("missing ipaddr");

    connect(_clap_matches.occurrences_of("verbose"), ipaddr);
}
