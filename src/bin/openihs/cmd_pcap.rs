//! Feature enabling the use of PCAP-NG files for debugging/testing

use std::boxed::Box;
use std::fs;
use std::path;
use std::sync::Arc;

use openihs::{string_range, packet::IHSPacket};
use parking_lot::RwLock;
use pnet::{self, packet::Packet};
use nom::{self, HexDisplay};

fn get_flow_string(data: &[u8]) -> String {
    let eth = pnet::packet::ethernet::EthernetPacket::new(data);
    if eth.is_some() {
        let eth = eth.unwrap();
        let eth_proto = eth.get_ethertype();
        if eth_proto == pnet::packet::ethernet::EtherTypes::Ipv4 {
            let _ip = pnet::packet::ipv4::Ipv4Packet::new(eth.payload());
            if _ip.is_some() {
                let ip = _ip.unwrap();
                let ip_proto = ip.get_next_level_protocol();
                if ip_proto == pnet::packet::ip::IpNextHeaderProtocols::Udp {
                    let _udp = pnet::packet::udp::UdpPacket::new(ip.payload());
                    if _udp.is_some() {
                        let udp = _udp.unwrap();

                        return String::from(format!(
                            "{:#?}:{} -> {:#?}:{}",
                            ip.get_source(),
                            udp.get_source(),
                            ip.get_destination(),
                            udp.get_destination()
                        ));
                    }
                }
            }
        }
    }
    String::from("INVALID IP HEADER")
}

// Parse given bytes out of the data slice as IHS UDP packet data
fn strip_frames(data: &[u8]) -> Option<Vec<u8>> {
    let eth = pnet::packet::ethernet::EthernetPacket::new(data);
    if eth.is_some() {
        let eth = eth.unwrap();

        // TODO: eth padding bytes... not sure how to easily detect and remove them
        // FCS is stripped by (most) nics so <64?

        let eth_proto = eth.get_ethertype();
        if eth_proto == pnet::packet::ethernet::EtherTypes::Ipv4 {
            let _ip = pnet::packet::ipv4::Ipv4Packet::new(eth.payload());
            if _ip.is_some() {
                let ip = _ip.unwrap();
                return strip_transport_frame(ip.get_next_level_protocol(), ip.payload());
            }
        } else if eth_proto == pnet::packet::ethernet::EtherTypes::Ipv6 {
            // TODO: ipv6
        } else {
            // Maybe a 'linux cooked capture' which replaces the eth frame...
            let _ip = pnet::packet::ipv4::Ipv4Packet::new(&data[16..]);
            if _ip.is_some() {
                let ip = _ip.unwrap();
                return strip_transport_frame(ip.get_next_level_protocol(), ip.payload());
            } else {
                // TODO: IPv6
            }
        }
    }
    None
}

fn strip_transport_frame(proto: pnet::packet::ip::IpNextHeaderProtocol, proto_data: &[u8]) -> Option<Vec<u8>> {
    if proto == pnet::packet::ip::IpNextHeaderProtocols::Udp {
        let _udp = pnet::packet::udp::UdpPacket::new(proto_data);
        if _udp.is_some() {
            let udp = _udp.unwrap();
            let sport = udp.get_source();
            let dport = udp.get_destination();
            if (sport > 27030 && sport < 27040) || (dport > 27030 && dport < 27040) {
                let ihs_data = udp.payload();
                return Some(ihs_data.to_vec());
            }
        }
    } else if proto == pnet::packet::ip::IpNextHeaderProtocols::Tcp {
        // TODO: Does the TLS traffic even have IHS protocol inside?
    }

    None
}

fn parse_ihs_data(data: &[u8]) -> Option<Box<dyn openihs::packet::IHSPacket>> {
    let mut databytes = bytes::BytesMut::new();
    databytes.extend_from_slice(data);

    let ihs_discovery = openihs::packet::discovery::Discovery::decode(&databytes);
    if ihs_discovery.is_ok() {
        return Some(Box::new(ihs_discovery.unwrap()));
    } else {
        let mut databytes = bytes::BytesMut::new();
        databytes.extend_from_slice(data);
        let stream = openihs::packet::stream::Stream::decode(&databytes);
        if stream.is_ok() {
            return Some(Box::new(stream.unwrap()));
        }
    }
    None
}

pub fn subcommand_entry_pcap(clap_matches: &clap::ArgMatches) {
    let pcap_subcmd = clap_matches.subcommand_matches("pcap").expect("missing subcommand: pcap");
    let _log_level = clap_matches.occurrences_of("verbose");
    let replay_mode = pcap_subcmd.is_present("replay");
    let pcap_path = path::Path::new(pcap_subcmd.value_of("pcap").expect("Missing param: PCAP_FILE"));
    let pcap_meta = fs::metadata(&pcap_path);
    if pcap_meta.is_err() || !pcap_meta.unwrap().is_file() {
        panic!("Failed to open file: {}", pcap_path.display());
    }
    let pcap_file = fs::File::open(&pcap_path).unwrap();
    let mut pcap_pcarp = pcarp::Capture::new(&pcap_file).expect("failed to parse file as pcapng");

    // Determine what range of packets should be read from the PCAP
    let pcap_range_param = pcap_subcmd.value_of("range");
    let mut pcap_range = string_range::StringRange {
        // Default range is 0 to usize
        s: String::from(""),
        r: vec![string_range::Range {
            start: 0,
            stop: usize::max_value(),
        }],
        min: 0,
        max: usize::max_value(),
    };
    if pcap_range_param.is_some() {
        // User has given us a range to filter with
        let user_range = string_range::StringRange::new(String::from(pcap_range_param.unwrap()));
        if user_range.is_ok() {
            pcap_range = user_range.unwrap();
        }
    }

    let iclient = if replay_mode {
        let (send, recv) = crossbeam::unbounded();
        let settings = openihs::client::settings::ClientSettings::load().expect("Failed to load client settings");
        let servers = openihs::client::settings::get_servers_hashmap().expect("Failed to load servers list");
        let (tcmd_send, _tcmd_recv) = crossbeam::unbounded();

        // The client methods expect IClient to be thread safe, so we gotta keep the Arc<RwLock<>> overhead...
        Some(Arc::new(RwLock::new(openihs::client::IClient {
            id: settings.id,
            request_id: rand::random::<u32>(),
            session_key: None,
            settings,
            servers,
            stream_socket: None,
            state: openihs::client::ConnectionState::Offline,
            auth_pin: None,
            event_receiver: Arc::new(recv.clone()),
            event_sender: Arc::new(send.clone()),
            thread_command: tcmd_send,
        })))
    } else { None };

    let mut i = 1;
    while let Some(pkt) = pcap_pcarp.next() {
        // Skip packets not in range parameter
        if pcap_range_param.is_none() || pcap_range.contains(i) {
            let pkt = pkt.unwrap();
            if _log_level > 3 {
                // trace level
                print!("DEBUG: Raw packet #{}\n{}", i, pkt.data.to_hex(16));
            }

            if replay_mode {
                let ihs_data = strip_frames(pkt.data);
                if ihs_data.is_some() {
                    match openihs::client::thread::parse_packet(iclient.as_ref().unwrap(), &ihs_data.unwrap()) {
                        openihs::client::events::ClientEvent::None => (),
                        e => {
                            println!("{:#?}", e);
                            std::thread::sleep(std::time::Duration::new(0, 500_000)); // Fake latency (0.5ms)
                        },
                    }
                }
            } else {
                let ihs_data = strip_frames(pkt.data);
                let ihs_packet = if ihs_data.is_some() { parse_ihs_data(&ihs_data.unwrap()) } else { None };
                if ihs_packet.is_some() {
                    let ips_string = get_flow_string(pkt.data);
                    println!("Packet #{} [{}]: {:#?}", i, ips_string, ihs_packet.unwrap().as_ref());
                }
            }
        }
        i += 1;
    }
}
