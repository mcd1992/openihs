use log::debug;
use openihs::client;
use openihs::client::events::ClientEvent;
use openihs::proto::discovery::CMsgRemoteClientBroadcastStatus;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

pub static EXIT_LOOP: AtomicBool = AtomicBool::new(false);

pub fn discover_servers(_log_level: u64) {
    let cstate = client::new().expect("Failed to initialize ClientState");

    ctrlc::set_handler(|| {
        println!("Exiting...");
        EXIT_LOOP.store(true, Ordering::Relaxed);
    }).expect("Error setting Ctrl-C handler");

    while !EXIT_LOOP.load(Ordering::Relaxed) {
        if let Ok(ClientEvent::ServerFound(server)) = cstate.event_recv_timeout(std::time::Duration::new(1, 0)) {
            let body: &CMsgRemoteClientBroadcastStatus = server.body.as_ref().unwrap().as_any().downcast_ref::<CMsgRemoteClientBroadcastStatus>().unwrap();
            println!("Found server: {} {}", body.get_ip_addresses().join(","), body.get_hostname());
            debug!("{:#?}", server);
        }
    }

    cstate.shutdown();
}

pub fn subcommand_entry_scan(_clap_matches: &clap::ArgMatches) {
    discover_servers(_clap_matches.occurrences_of("verbose"));
}
