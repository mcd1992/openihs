use clap::{App, AppSettings, Arg, SubCommand};
use log::error;
use stderrlog;

pub mod cmd_connect;
pub mod cmd_scan;

#[cfg(feature = "pcapng")]
pub mod cmd_pcap;

fn main() {
    let mut app = App::new("OpenIHS Client")
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .setting(AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("CONFIG_FILE")
                .help("Sets a custom config file"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Sets the level of verbosity. Use multiple v's for more verbose output\n[1=INFO, 2=DEBUG, 3=TRACE]"),
        )
        .subcommand(
            SubCommand::with_name("scan")
                .about("Send a STEAMDISCOVER broadcast to find IHS servers")
                .arg(
                    Arg::with_name("interface")
                        .short("i")
                        .long("interface")
                        .value_name("INTERFACE")
                        .help("Send broadcast out selected interface only"),
                ),
        )
        .subcommand(
            SubCommand::with_name("connect")
                .about("Connect to an IHS server by IP")
                .arg(
                    Arg::with_name("ipaddr")
                        .value_name("IPADDR")
                        .index(1)
                        .required(true)
                        .help("IP address of the IHS server to connect to"),
                )
        );

    app = add_feature_pcapng(app);
    let matches = app.get_matches();
    stderrlog::new()
        //.module(module_path!())
        .quiet(false)
        .verbosity(matches.occurrences_of("verbose") as usize + 1)
        .timestamp(stderrlog::Timestamp::Off)
        .init()
        .unwrap();

    match matches.subcommand_name() {
        Some("scan") => {
            cmd_scan::subcommand_entry_scan(&matches);
        }
        #[cfg(feature = "pcapng")]
        Some("pcap") => {
            cmd_pcap::subcommand_entry_pcap(&matches);
        }
        Some("connect") => {
            cmd_connect::subcommand_entry_connect(&matches);
        }
        cmd @ _ => {
            error!("Command not implemented: {}\nUse --help for more info.", cmd.unwrap_or("None"));
        }
    }
}

#[cfg(not(feature = "pcapng"))]
fn add_feature_pcapng<'a, 'b>(app: App<'a, 'b>) -> App<'a, 'b> {
    app
}
#[cfg(feature = "pcapng")]
fn add_feature_pcapng<'a, 'b>(app: App<'a, 'b>) -> App<'a, 'b> {
    app.subcommand(
        SubCommand::with_name("pcap")
            .about("Use a pcap-ng capture file for replay/testing")
            .arg(
                Arg::with_name("range")
                    .short("r")
                    .long("range")
                    .value_name("PCAP_RANGE")
                    .help("Only read packets within the given range [2-4,8,11]"),
            )
            .arg(
                Arg::with_name("replay")
                    .short("R")
                    .long("replay")
                    .takes_value(false)
                    .help("Replay the packets into the Client interface, emulating a real connection"),
            )
            .arg(
                Arg::with_name("pcap")
                    .value_name("PCAP_FILE")
                    .index(1)
                    .required(true)
                    .help("Read the given pcapng file for testing"),
            ),
    )
}
