use crate::packet::discovery;
use crate::packet::stream;

// IO-ed with the main packet handling thread to schedule / receive events
#[derive(Debug)]
pub enum ClientEvent {
    // System events
    Shutdown,   // Network thread is shutting down
    None,       // Used internally in the net thread for non-ihs data (or ignorable data)

    // Discovery events
    ServerFound(discovery::Discovery),
    StreamResponse(discovery::Discovery),
    AuthorizationRequestPIN(String),
    AuthorizationResponse(discovery::Discovery),
    DeviceProofRequest(discovery::Discovery),

    // Stream events
    StreamSynAcked,
    StreamMTURequest(stream::Stream),
    StreamServerHandshake(stream::Stream),
    StreamAuthenticationResponse(stream::Stream),
    StreamNegotiationInit(stream::Stream),
    StreamNegotiationSetConfig(stream::Stream),
    StreamSetQoS(stream::Stream),
    StreamSetTargetBitrate(stream::Stream),
    StreamStartAudioData(stream::Stream),
    StreamSetSpectatorMode(stream::Stream),
    StreamSetTitle(stream::Stream),
    StreamSetIcon(stream::Stream),
    StreamShowCursor(stream::Stream),
    StreamSetActivity(stream::Stream),
    StreamStartVideoData(stream::Stream),
    StreamVideoEncoderInfo(stream::Stream),
    StreamUnknown(stream::Stream),

    StreamLogMessage(stream::Stream),
}
