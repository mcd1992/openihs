#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use std::collections::HashMap;
use std::sync::{Arc, atomic::Ordering};
use parking_lot::RwLock;
use crate::server::ServerInfo;
use crate::client::events::ClientEvent;
use crate::client::thread::ThreadCommand;

pub mod events;
pub mod settings;
pub mod thread;

// Re-exports for visibility
pub fn new() -> Result<Client, Box<dyn std::error::Error>> {
    Client::new()
}

#[derive(Debug)]
pub enum ConnectionState { // What state the client is in. Used for packet responses.
    Uninitialized = -1, // Client thread loop needs to be started
    Offline, // Client is doing nothing / k_ERemoteClientBroadcastMsgOffline (ignore incoming data)

    Discovery, // Client sent k_ERemoteClientBroadcastMsgDiscovery (listen for k_ERemoteClientBroadcastMsgStatus and update available_servers)
    DiscoveryRequestingStream, // Client sent k_ERemoteDeviceStreamingRequest (listen for k_ERemoteDeviceStreamingResponse and set next state as needed)
    DiscoveryRequestingAuth,   // Client sent k_ERemoteDeviceAuthorizationRequest (listen for k_ERemoteDeviceAuthorizationResponse and set next state as needed)
    DiscoveryRespondingProof,  // Server sent k_ERemoteDeviceProofRequest (reply with k_ERemoteDeviceProofResponse and then go back to RequestingStream)
    DiscoveryInitializeStream, // Everything is setup on the control side. Now we need to establish a connection on 27031 (or whatever was negotiated)

    StreamSyn,
    StreamClientHandshake,
    StreamMTU,
    StreamServerHandshake,
    StreamAuth,
    StreamNegotiationInit,
    StreamNegotiationSetConfig,

    Streaming, // The client is currently streaming with a server!
}

//
// Internal type for dropping when no references exist
//
#[derive(Debug)]
struct ClientDrop;

impl Drop for ClientDrop {
    fn drop(&mut self) {
        trace!("ClientDrop::drop called");
        thread::NET_THREAD_RUNNING.store(false, Ordering::Relaxed);
    }
}
//
//
//


//
// 'Public' interface for the library
//
#[derive(Debug)]
pub struct Client {
    iclient: Arc<RwLock<IClient>>,
    handle: Option<std::thread::JoinHandle<()>>,
    event_receiver: crossbeam::Receiver<ClientEvent>,
    drop: Arc<ClientDrop>, // Private ref. counter for killing the net thread when no refs. exist
}

impl Client {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        let thread_running = thread::NET_THREAD_RUNNING.swap(true, Ordering::Relaxed);
        if thread_running {
            panic!("you can only have one Client state at a time");
        } else {
            let (send, recv) = crossbeam::unbounded();
            let settings = settings::ClientSettings::load()?;
            let servers = settings::get_servers_hashmap()?;
            let (tcmd_send, tcmd_recv) = crossbeam::unbounded();

            let iclient = Arc::new(RwLock::new(IClient {
                id: settings.id,
                request_id: rand::random::<u32>(),
                session_key: None,
                settings,
                servers,

                stream_socket: None,
                state: ConnectionState::Offline,
                auth_pin: None,

                event_receiver: Arc::new(recv.clone()),
                event_sender: Arc::new(send.clone()),
                thread_command: tcmd_send,
            }));

            let handle = thread::start(iclient.clone(), tcmd_recv)?;
            Ok(Client {
                iclient,
                event_receiver: recv.clone(),
                handle: Some(handle),
                drop: Arc::new(ClientDrop),
            })
        }
    }

    pub fn event_recv(&self) -> Result<events::ClientEvent, crossbeam::channel::RecvError> {
        self.event_receiver.recv()
    }
    pub fn event_recv_timeout(&self, timeout: std::time::Duration) -> Result<events::ClientEvent, crossbeam::channel::RecvTimeoutError> {
        self.event_receiver.recv_timeout(timeout)
    }

    pub fn connect(&self, server: &ServerInfo) {
        // TODO: handle all the connection logic for the user

    }

    pub fn send_discovery(&self) -> Result<(), crossbeam::TrySendError<ThreadCommand>> {
        self.iclient.read().thread_command.try_send(ThreadCommand::SendDiscoveryBroadcast)
    }

    pub fn send_stream_request(&self, server: &ServerInfo) -> Result<(), crossbeam::TrySendError<ThreadCommand>> {
        self.iclient.read().thread_command.try_send(ThreadCommand::SendStreamRequest(server.clone()))
    }

    pub fn send_authorization_request(&self, server: &ServerInfo) -> Result<(), crossbeam::TrySendError<ThreadCommand>> {
        self.iclient.read().thread_command.try_send(ThreadCommand::SendAuthorizationRequest(server.clone()))
    }

    pub fn send_proof_response(&self, server: &ServerInfo, req_pkt: crate::packet::discovery::Discovery) -> Result<(), crossbeam::TrySendError<ThreadCommand>> {
        self.iclient.read().thread_command.try_send(ThreadCommand::SendProofResponse(server.clone(), req_pkt))
    }

    pub fn get_servers(&self) -> HashMap<u64, ServerInfo> {
        self.iclient.read().servers.clone()
    }

    pub fn save_settings(&self) {
        let iclient = self.iclient.read();
        let _ = iclient.settings.save();
        let _ = settings::save_servers_hashmap(&iclient.servers);
    }

    pub fn shutdown(mut self) {
        self.save_settings();
        thread::NET_THREAD_RUNNING.store(false, Ordering::Relaxed);
        let _ = self.handle.take().unwrap().join();
    }
}
//
//
//



//
// 'Internal' interface used in the network thread and interfaced with the public methods
//
#[derive(Debug)]
pub struct IClient {
    pub id: u64,
    pub request_id: u32,
    pub session_key: Option<[u8; 16]>,
    pub settings: settings::ClientSettings,
    pub servers: HashMap<u64, ServerInfo>,

    pub stream_socket: Option<std::net::UdpSocket>,
    pub state: ConnectionState, // TODO: bitflag
    pub auth_pin: Option<String>,

    pub event_sender: Arc<crossbeam::Sender<ClientEvent>>,
    pub event_receiver: Arc<crossbeam::Receiver<ClientEvent>>,
    pub thread_command: crossbeam::Sender<ThreadCommand>
}
//
//
//
