#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use std::net::UdpSocket;
use std::sync::{Arc, atomic};
use parking_lot::RwLock;
use std::collections::HashMap;
use bytes::BytesMut;
use chrono::Utc;
use protobuf::Message;

use crate::packet;
use crate::proto::discovery;
use crate::proto::discovery::ERemoteClientBroadcastMsg;
use crate::packet::IHSPacket;
use crate::client::IClient;
use crate::client::events::ClientEvent;
use crate::server::ServerInfo;

// Thread loop signal
pub static NET_THREAD_RUNNING: atomic::AtomicBool = atomic::AtomicBool::new(false);
const CLIENT_HOSTNAME: &str = "OpenIHS"; // TODO: hostname

#[derive(Debug)]
pub enum ThreadCommand {
    SendDiscoveryBroadcast,
    SendStreamRequest(ServerInfo),
    SendAuthorizationRequest(ServerInfo),
    SendProofResponse(ServerInfo, packet::discovery::Discovery)
}

//
// Private methods
//
fn _encrypt_device_token(iclient: &Arc<RwLock<IClient>>, server: &ServerInfo) -> Vec<u8> {
    let iclient = iclient.read();
    let aes_key = server.key;
    let token = iclient.settings.id.to_le_bytes();

    crate::packet::symmetric_encrypt_random_iv(&token.to_vec(), &aes_key.to_vec()).expect("Failed to encrypt device token")
}

fn _encrypt_proof_response(_iclient: &Arc<RwLock<IClient>>, server: &ServerInfo, challenge: &[u8]) -> Vec<u8> {
    let aes_key = server.key;

    crate::packet::symmetric_encrypt_random_iv(&challenge.to_vec(), &aes_key.to_vec()).expect("Failed to encrypt proof request")
}
//
//
//



//
// Send methods
//
fn send_discovery(iclient: &Arc<RwLock<IClient>>, sock: &UdpSocket) {
    let mut bcast: packet::discovery::Discovery = Default::default();
    bcast.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    bcast.header.set_client_id(iclient.read().id);
    bcast.header.set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgDiscovery);
    bcast.body = Some(Box::new(discovery::CMsgRemoteClientBroadcastDiscovery::new()));

    let mut buf = bytes::BytesMut::new();
    let _ = bcast.encode(&mut buf).unwrap();
    let _ = sock.send_to(&buf.to_vec(), "255.255.255.255:27036").unwrap();
}

fn send_stream_request(iclient: &Arc<RwLock<IClient>>, sock: &UdpSocket, server: &ServerInfo) {
    let client_id = iclient.read().settings.id;
    let mut pkt: packet::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(client_id);
    pkt.header.set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceStreamingRequest);

    // Setup streaming request body
    let mut body = discovery::CMsgRemoteDeviceStreamingRequest::new();
    body.set_request_id(iclient.read().request_id);
    body.set_maximum_resolution_x(960);
    body.set_maximum_resolution_y(540);
    body.set_audio_channel_count(2);
    body.set_device_version(format!("{}{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION")).to_string());
    body.set_stream_desktop(true);
    body.set_device_token(_encrypt_device_token(&iclient, &server));
    body.set_pin("".as_bytes().to_vec());
    body.set_enable_video_streaming(true);
    body.set_enable_audio_streaming(true);
    body.set_enable_input_streaming(true);
    body.set_network_test(false);
    body.set_client_id(server.id); // This 'client_id' refers to the 'server's client_id that we want to connect to
    body.set_supported_transport(vec![discovery::EStreamTransport::k_EStreamTransportUDP]);
    body.set_restricted(false); // Not sure what this does
    body.set_form_factor(discovery::EStreamDeviceFormFactor::k_EStreamDeviceFormFactorPhone);
    body.set_gamepad_count(0);
    body.set_gameid(0);
    body.set_stream_interface(discovery::EStreamInterface::k_EStreamInterfaceDesktop);

    pkt.body = Some(Box::new(body));

    let mut buf = bytes::BytesMut::new();
    let _ = pkt.encode(&mut buf).unwrap(); // TODO: optimize by doing this on stack or maybe even vec?

    // TODO: need to target whatever was passed in cmd_connect (or check whichever IP is actually reachable?)
    let ipaddr = &server.ip_addresses[0];
    let _ = sock.send_to(&buf.to_vec(), format!("{}:27036", ipaddr)).unwrap();
}

fn send_authorization_request(iclient: &Arc<RwLock<IClient>>, sock: &UdpSocket, server: &ServerInfo) {
    let client_id = iclient.read().settings.id;
    let mut pkt: packet::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(client_id);
    pkt.header.set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceAuthorizationRequest);

    // Setup authorization request body
    let mut body = discovery::CMsgRemoteDeviceAuthorizationRequest::new();
    body.set_device_name(String::from(CLIENT_HOSTNAME));
    body.set_device_token(_encrypt_device_token(&iclient, &server));

    // Create an 'escrow' ticket to be proxied through steam running on the 'ihs server' so the escrow can be decrypted by valve
    let mut escrow = discovery::CMsgRemoteDeviceAuthorizationRequest_CKeyEscrow_Ticket::new();
    escrow.set_password("1111".as_bytes().to_vec()); // TODO: need to get this from lib user
    escrow.set_identifier(client_id);
    escrow.set_payload(server.key.to_vec());
    // timestamp?
    escrow.set_usage(discovery::CMsgRemoteDeviceAuthorizationRequest_EKeyEscrowUsage::k_EKeyEscrowUsageStreamingDevice);
    escrow.set_device_name(String::from(CLIENT_HOSTNAME));

    // Optional fields
    //escrow.set_device_model("1003".to_string());
    //escrow.set_device_serial("FL552027E3".to_string()); // The last 2 bytes of the serial will be first 2 bytes in token
    //escrow.set_device_provisioning_id(395605); // last part of device_id.txt, used in token though!
    let escrow_bytes = escrow.write_to_bytes().expect("Failed to write escrow ticket to bytes");

    // RSA encrypt with valve's pubkey
    let escrow_enc = crate::packet::rsa_encrypt(&escrow_bytes).expect("Failed to encrypt escrow protobuf");
    body.set_encrypted_request(escrow_enc);

    pkt.body = Some(Box::new(body));
    let mut buf = bytes::BytesMut::new();
    let _ = pkt.encode(&mut buf).unwrap();

    // TODO: need to target whatever was passed in cmd_connect (or check whichever IP is actually reachable?)
    let ipaddr = &server.ip_addresses[0];
    let _ = sock.send_to(&buf.to_vec(), format!("{}:27036", ipaddr)).unwrap();
}

fn send_proof_response(iclient: &Arc<RwLock<IClient>>, sock: &UdpSocket, server: &ServerInfo, request: &packet::discovery::Discovery) {
    let client_id = iclient.read().settings.id;
    let mut pkt: packet::discovery::Discovery = Default::default();
    pkt.header = discovery::CMsgRemoteClientBroadcastHeader::new();
    pkt.header.set_client_id(client_id);
    pkt.header.set_msg_type(discovery::ERemoteClientBroadcastMsg::k_ERemoteDeviceProofResponse);

    // Setup authorization request body
    let mut body = discovery::CMsgRemoteDeviceProofResponse::new();
    let req_body: &discovery::CMsgRemoteDeviceProofRequest = request.body.as_ref().unwrap().as_any().downcast_ref::<discovery::CMsgRemoteDeviceProofRequest>().unwrap();
    body.set_response(_encrypt_proof_response(&iclient, &server, req_body.get_challenge()));
    body.set_request_id(req_body.get_request_id());

    pkt.body = Some(Box::new(body));
    let mut buf = bytes::BytesMut::new();
    let _ = pkt.encode(&mut buf).unwrap();

    // TODO: need to target whatever was passed in cmd_connect (or check whichever IP is actually reachable?)
    let ipaddr = &server.ip_addresses[0];
    let _ = sock.send_to(&buf.to_vec(), format!("{}:27036", ipaddr)).unwrap();
}

//
// Parse methods
//
fn parse_server_status(iclient: &Arc<RwLock<IClient>>, pkt: &packet::discovery::Discovery) {
    let mut iclient = iclient.write(); // grab a write lock since we'll always be writing at the end of this method
    let id = pkt.header.get_client_id();
    let body = pkt.body.as_ref().unwrap().as_any().downcast_ref::<discovery::CMsgRemoteClientBroadcastStatus>().unwrap();
    let epoch = Utc::now();
    let server_map = &iclient.servers;
    let old_server_info = server_map.get(&id);

    // Load previous aes_key or create new one
    let aes_key = if old_server_info.is_none() {
        // New server being added generate new aes key
        let mut new_key = [0u8; 32];
        let _ = openssl::rand::rand_bytes(&mut new_key);
        new_key
    } else {
        *&old_server_info.unwrap().key
    };

    // Add any new users or create new map
    let mut users: HashMap<u64, u32> = if old_server_info.is_some() {
        old_server_info.unwrap().users.clone()
    } else {
        HashMap::new()
    };
    for u in body.get_users() {
        // discovery::CMsgRemoteClientBroadcastStatus_User
        users.insert(u.get_steamid(), u.get_auth_key_id());
    }

    let stream_port = 27031; // does this ever change? its passed in
    let server = crate::server::ServerInfo {
        // We need to keep track of these things ourselves
        last_seen: epoch.timestamp() as u64,
        key: aes_key,
        stream_port,

        // The below members are safe to set directly from the CMsgRemoteClientBroadcastStatus response
        id,
        instance_id: pkt.header.get_instance_id(),
        version: body.get_version(),
        min_version: body.get_min_version(),
        connect_port: body.get_connect_port(),
        hostname: body.get_hostname().to_string(),
        enabled_services: body.get_enabled_services(),
        ostype: body.get_ostype(),
        is64bit: body.get_is64bit(),
        users,
        euniverse: body.get_euniverse(),
        timestamp: body.get_timestamp(),
        screen_locked: body.get_screen_locked(),
        games_running: body.get_games_running(),
        mac_addresses: body.get_mac_addresses().to_vec(),
        download_lan_peer_group: body.get_download_lan_peer_group(),
        broadcasting_active: body.get_broadcasting_active(),
        vr_active: body.get_vr_active(),
        content_cache_port: body.get_content_cache_port(),
        ip_addresses: body.get_ip_addresses().to_vec(),
        public_ip_address: body.get_public_ip_address().to_string(),
        remoteplay_active: body.get_remoteplay_active(),
    };
    iclient.servers.insert(id, server);
}

fn parse_stream_response(_iclient: &Arc<RwLock<IClient>>, pkt: &packet::discovery::Discovery) {
    //let mut iclient = iclient.write(); // grab a write lock since we'll always be writing at the end of this method
    //let id = pkt.header.get_client_id();
    let body: &discovery::CMsgRemoteDeviceStreamingResponse = pkt.body.as_ref().unwrap().as_any().downcast_ref::<discovery::CMsgRemoteDeviceStreamingResponse>().unwrap();
    let response = body.get_result();

    match response {
        /*k_ERemoteDeviceStreamingSuccess => (),
        k_ERemoteDeviceStreamingUnauthorized => (),
        k_ERemoteDeviceStreamingScreenLocked => (),
        k_ERemoteDeviceStreamingFailed => (),
        k_ERemoteDeviceStreamingBusy => (),
        k_ERemoteDeviceStreamingInProgress => (),
        k_ERemoteDeviceStreamingCanceled => (),
        k_ERemoteDeviceStreamingDriversNotInstalled => (),
        k_ERemoteDeviceStreamingDisabled => (),
        k_ERemoteDeviceStreamingBroadcastingActive => (),
        k_ERemoteDeviceStreamingVRActive => (),
        k_ERemoteDeviceStreamingPINRequired => (),
        k_ERemoteDeviceStreamingTransportUnavailable => (),
        k_ERemoteDeviceStreamingInvisible => (),
        k_ERemoteDeviceStreamingGameLaunchFailed => (),*/
        _ => (),
    }
}

pub fn parse_packet(iclient: &Arc<RwLock<IClient>>, data: &[u8]) -> ClientEvent {
    if let Ok(pkt) = packet::discovery::Discovery::decode(&BytesMut::from(data)) {
        match pkt.header.get_msg_type() {
            ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgStatus => {
                parse_server_status(&iclient, &pkt);
                return ClientEvent::ServerFound(pkt);
            }
            ERemoteClientBroadcastMsg::k_ERemoteDeviceStreamingResponse => {
                parse_stream_response(&iclient, &pkt);
                return ClientEvent::StreamResponse(pkt);
            }
            ERemoteClientBroadcastMsg::k_ERemoteDeviceAuthorizationResponse => {
                return ClientEvent::AuthorizationResponse(pkt);
            }
            ERemoteClientBroadcastMsg::k_ERemoteDeviceProofRequest => {
                return ClientEvent::DeviceProofRequest(pkt);
            }
            t => {
                debug!("[NET_THREAD] Unhandled packet type in parse_packet: {:#?}", t);
            },
        }
    }

    ClientEvent::None
}

//
// Main thread
//
pub fn start(iclient: Arc<RwLock<IClient>>, thread_command: crossbeam::Receiver<ThreadCommand>) -> Result<std::thread::JoinHandle<()>, Box<dyn std::error::Error>> {
    // Setup our sockets
    let dsock = UdpSocket::bind("0.0.0.0:0")?;
    dsock.set_broadcast(true)?;
    dsock.set_read_timeout(Some(std::time::Duration::new(0, 250_000_000)))?; // seconds, useconds
    let ssock: Option<UdpSocket> = None;

    // Clone any needed variables in the thread
    let handle = std::thread::spawn(move || {
        trace!("[NET_THREAD] Sending initial discovery broadcast");
        send_discovery(&iclient, &dsock);

        let thread_command_timeout = std::time::Duration::new(0, 250_000_000);
        while NET_THREAD_RUNNING.load(atomic::Ordering::Relaxed) {
            // TODO: async loop?

            let mut recvbuf = [0u8; 8192];
            if let Ok((size, _peer)) = dsock.recv_from(&mut recvbuf) {
                //trace!("[NET_THREAD] recv_from read {} bytes from {}:{}", _size, _peer.ip(), _peer.port());
                match parse_packet(&iclient, &recvbuf[..size]) {
                    ClientEvent::None => (),
                    e => {
                        let _ = iclient.read().event_sender.try_send(e);
                    }
                }
            }

            // Command loop. Used by the client methods to tell the thread to send packets from here
            if let Ok(cmd) = thread_command.recv_timeout(thread_command_timeout) { // TODO: this will be a bottleneck on IO, just testing for now though
                match cmd {
                    ThreadCommand::SendDiscoveryBroadcast => send_discovery(&iclient, &dsock),
                    ThreadCommand::SendStreamRequest(server) => send_stream_request(&iclient, &dsock, &server),
                    ThreadCommand::SendAuthorizationRequest(server) => send_authorization_request(&iclient, &dsock, &server),
                    ThreadCommand::SendProofResponse(server, req_pkt) => send_proof_response(&iclient, &dsock, &server, &req_pkt),
                }
            }
        }
        let _ = iclient.read().event_sender.send(ClientEvent::Shutdown);
        trace!("[NET_THREAD] Exiting network thread loop...");
    });
    Ok(handle)
}
