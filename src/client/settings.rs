#[allow(unused_imports)]
use log::{error, warn, info, debug, trace};

use std::path::PathBuf;
use std::collections::HashMap;
use std::fs::{File, create_dir_all};
use std::io::prelude::*; // .write_all
use serde::{Serialize, Deserialize};
use directories::ProjectDirs;
use rand::random;

use crate::server::ServerInfo;

fn _get_config_file_path(filename: &str) -> PathBuf {
    let mut path = PathBuf::from(".");
    path.push(filename);
    if !path.exists() { // Check if this file exists in the CWD first
        let projdir = ProjectDirs::from("", "", env!("CARGO_PKG_NAME"));
        if projdir.is_some() {
            path = projdir.unwrap().config_dir().to_path_buf();
            create_dir_all(&path).expect("Failed to create config directory");
            path.push(filename);
        }
    }
    path
}

pub fn get_servers_hashmap() -> Result<HashMap<u64, ServerInfo>, Box<dyn std::error::Error>> {
    // Load RON serialized servers
    let path = _get_config_file_path("servers.ron");
    let statefile = File::open(&path);
    if statefile.is_ok() {
        let f = statefile.unwrap();
        let stateobj = ron::de::from_reader::<File, HashMap<u64, ServerInfo>>(f);
        if stateobj.is_ok() {
            debug!("Successfully loaded saved servers from {:#?}", &path);
            return Ok(stateobj.unwrap());
        }
    }
    Ok(HashMap::new())
}

pub fn save_servers_hashmap(servers: &HashMap<u64, ServerInfo>) -> Result<(), Box<dyn std::error::Error>> {
    // Save serialized servers and settings to os app dir
    let statedata = ron::ser::to_string_pretty(
        servers,
        ron::ser::PrettyConfig::new())
    .expect("Failed to serialize servers");

    // Save servers
    let path = _get_config_file_path("servers.ron");
    let mut statefile = File::create(&path).expect("Failed to open state file for saving");
    statefile.write_all(statedata.as_bytes()).expect("Failed to write servers.ron");
    debug!("Wrote statedata to {:#?}", &path);
    Ok(())
}

// This is serialized into RON for user settings storage on the fs
#[derive(Debug, Serialize, Deserialize)]
pub struct ClientSettings {
    pub id: u64,
}

impl ClientSettings {
    pub fn load() -> Result<Self, Box<dyn std::error::Error>> {
        // Load RON serialized settings
        let path = _get_config_file_path("settings.ron");
        let settingsfile = File::open(&path);
        if settingsfile.is_ok() {
            let f = settingsfile.unwrap();
            let settingsobj = ron::de::from_reader::<File, ClientSettings>(f);
            if settingsobj.is_ok() {
                debug!("Successfully loaded saved settings from {:#?}", &path);
                return Ok(settingsobj.unwrap());
            }
        }
        Ok(ClientSettings {
            id: random::<u64>(),
        })
    }

    pub fn save(&self) -> Result<(), Box<dyn std::error::Error>> {
        // Save settings as serialized RON
        let settingsdata = ron::ser::to_string_pretty(
            &self,
            ron::ser::PrettyConfig::new()
        ).expect("Failed to serialize ClientSettings");

        let path = _get_config_file_path("settings.ron");
        let mut settingsfile = File::create(&path).expect("Failed to open settings file for saving");
        settingsfile.write_all(settingsdata.as_bytes()).expect("Failed to write settings.ron");
        debug!("Wrote settingsdata to {:#?}", &path);

        Ok(())
    }
}