use serde::{Serialize, Deserialize};
use std::collections::HashMap;

// Represents an 'IHS server' (device running steam that we can stream from)
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServerInfo {
    // Data found in CMsgRemoteClientBroadcastHeader
    pub id: u64,           // This 'server's ID (called client_id in header!)
    pub instance_id: u64,  // Session unique random ID
    // pub device_id: u64, // Seems to be unused?
    // pub device_token: [u8; 8], // Not sure if we need this stored?

    // Data found in CMsgRemoteClientBroadcastStatus
    pub version: i32,           // Servers version number (8 seems to be latest as of 2020-06-06)
    pub min_version: i32,       // Servers minimum supported version (6-8 seems to be compat.)
    pub connect_port: u32,      // Target UDP port for discovery data (Always 27036?)
    pub hostname: String,
    // missing protobuf field (5)
    pub enabled_services: u32,
    pub ostype: i32,
	pub is64bit: bool,
    pub users: HashMap<u64, u32>, // SteamID64 and 'auth_key_id'?
    // missing protobuf field (10)
	pub euniverse: i32,
	pub timestamp: u32,
	pub screen_locked: bool,
	pub games_running: bool,
	pub mac_addresses: Vec<String>,
	pub download_lan_peer_group: u32,
	pub broadcasting_active: bool,
	pub vr_active: bool,
	pub content_cache_port: u32,
    pub ip_addresses: Vec<String>,
	pub public_ip_address: String,
	pub remoteplay_active: bool,

    // Custom data
    pub last_seen: u64,   // Epoch in UTC time
    pub key: [u8; 32],    // Users aes-256-cbc symmetric key
    pub stream_port: u16, // Target UDP port for stream data (Received in CMsgRemoteDeviceStreamingResponse)
}
