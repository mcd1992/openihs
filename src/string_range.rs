//! Take a string like `1-4,6,5,2-10` and sort+uniq it
// Return a struct that has method: contains(usize) -> bool
// This is used by the pcapng feature for targeting a selected range of packets in the pcap

use regex::Regex;

// Generic trait for 'function overloading'
trait RangeContains<T> {
    fn contains(&self, other: T) -> bool;
}

/// Contains a range of usize integers (inclusive)
#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Range {
    pub start: usize,
    pub stop: usize,
}
impl RangeContains<&Range> for Range {
    fn contains(&self, other: &Self) -> bool {
        self.start <= other.start && self.stop >= other.stop
    }
}
impl RangeContains<usize> for Range {
    fn contains(&self, val: usize) -> bool {
        val >= self.start && val <= self.stop
    }
}

#[derive(Debug, Clone)]
pub struct StringRange {
    pub s: String,     // The string, given by the user, that this range represents
    pub r: Vec<Range>, // Contains all the ranges this string represents
    pub min: usize,    // Smallest value in this range
    pub max: usize,    // Largest value in this range
}
impl StringRange {
    pub fn new(user_string: String) -> Result<Self, Box<String>> {
        let range_regex = Regex::new(r"^[0-9\-,]+$").unwrap();
        if !range_regex.is_match(user_string.as_str()) {
            return Err(Box::new(format!("Invalid range parameter: {}", user_string)));
        }

        let split_comma = Regex::new(r",").unwrap();
        let split_dash = Regex::new(r"-").unwrap();
        let split_iter = split_comma.split(user_string.as_str());
        let mut min = usize::max_value();
        let mut max = 0;
        let mut dirty_ranges: Vec<Range> = split_iter
            .filter_map(|s| {
                #[allow(unused_assignments)] // Linter pls
                let mut start = 0;
                #[allow(unused_assignments)]
                let mut stop = 0;

                // Remove improper ranges
                if s.contains('-') {
                    let s_split = split_dash.splitn(s, 2);
                    let lr: Vec<&str> = s_split.collect();
                    let lval = lr[0].parse::<usize>();
                    let rval = lr[1].parse::<usize>();
                    start = if lval.is_ok() {
                        lval.unwrap()
                    } else {
                        return None;
                    };
                    stop = if rval.is_ok() {
                        rval.unwrap()
                    } else {
                        return None;
                    };
                } else {
                    // Single value 'range'
                    let val = s.parse::<usize>();
                    if val.is_ok() {
                        start = val.unwrap();
                        stop = start;
                    } else {
                        return None;
                    }
                }
                if start > stop {
                    return None;
                } // Backwards range, just ignore

                // Determine min/max
                min = if start < min { start } else { min };
                max = if stop > max { stop } else { max };

                // Create range object
                Some(Range { start, stop })
            })
            .collect();
        if dirty_ranges.is_empty() {
            return Err(Box::new(format!("Invalid range parameter: {}", user_string)));
        }

        // This will contain the 'clean' ranges
        let mut ranges: Vec<Range> = vec![];

        // Sort and merge overlapping ranges
        dirty_ranges.sort();
        let mut last: &Range = dirty_ranges.first().unwrap();
        for r in &dirty_ranges {
            if ranges.is_empty() {
                // Empty Vec, just push our first Range to get started
                ranges.push(r.to_owned());
            } else if last.contains(r) {
                // The last range completely overlaps with this; skip
            } else if r.start >= last.start && r.start <= last.stop && r.stop >= last.stop {
                // This range starts within/on the bounds of our last range; merge
                ranges.last_mut().unwrap().stop = r.stop;
            } else if r.start == (last.stop + 1) && r.stop >= last.stop {
                // This range is directly adjacent to the last; merge
                ranges.last_mut().unwrap().stop = r.stop;
            } else {
                // Looks like theres a gap between last.end and r.start; push
                ranges.push(r.to_owned());
            }
            last = ranges.last().unwrap();
        }
        Ok(StringRange {
            s: String::from(user_string),
            r: ranges,
            min,
            max,
        })
    }

    pub fn contains(&self, val: usize) -> bool {
        if val > self.max || val < self.min {
            return false;
        }

        // Iterate over all our ranges and see if any contain this value
        for r in &self.r {
            // TODO: Optimize?
            if r.contains(val) {
                return true;
            }
        }
        false
    }
}
