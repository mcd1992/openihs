//! An open-source, rust, implementation of the Steam In-Home Streaming protcol library
/*!
```rust
    // TODO: Example code
```
*/

// Export our library modules
pub mod packet;
pub mod proto;
pub mod client;
pub mod server;
pub mod string_range;
