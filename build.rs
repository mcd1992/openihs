extern crate protoc_rust;
use std::process::Command;
use std::time::Instant;

fn main() {
    println!("cargo:rerun-if-changed=protos/build.py");
    println!("cargo:rerun-if-changed=protos/discovery.proto");
    println!("cargo:rerun-if-changed=protos/stream.proto");

    //
    // Compile our protobuf files into Rust with prost
    //
    print!("Transpiling protobufs...");
    let start_time = Instant::now();
    protoc_rust::Codegen::new()
        .out_dir(concat!(env!("PWD"), "/src/proto/"))
        .inputs(&["protos/discovery.proto", "protos/stream.proto"])
        .include("protos")
        .run()
        .expect("Failed to transpile protobufs.");
    let elapsed = start_time.elapsed();
    let millis = elapsed.as_secs() * 1000 + elapsed.subsec_nanos() as u64 / 1_000_000;
    println!(" DONE! [{}ms]", millis);

    //
    // Build the terrible match statements for valve's protobuf enum to body mapping (protos/build.py)
    //
    print!("Building python protobuf files for enummap.rs...");
    let output = Command::new("protoc")
        .current_dir(format!("{}/protos/", env!("PWD")))
        .arg("--python_out=./")
        .arg("./discovery.proto")
        .arg("./stream.proto")
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        println!(" DONE! {}", String::from_utf8_lossy(&output.stdout));
        print!("Building enummap.rs...");
        let output = Command::new(format!("{}/protos/build.py", env!("PWD")))
            .current_dir(format!("{}/protos/", env!("PWD")))
            .output()
            .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
        if output.status.success() {
            println!(" DONE! {}", String::from_utf8_lossy(&output.stdout));
        } else {
            println!("ERROR!\n{}", String::from_utf8_lossy(&output.stderr));
        }
    } else {
        println!("ERROR!\n{}", String::from_utf8_lossy(&output.stderr));
    }
}
